﻿Shader "Custom/ImageEffect"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_guiTex("GUI Texture", 2D) = "white" {}
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
			sampler2D _guiTex;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 guiCol = tex2D(_guiTex, i.uv);

				if (length(guiCol) > 0.75f)
				{
					//Use YIQ to work out if it should make the UI black or white
					int r = col.r * 255;
					int g = col.g * 255;
					int b = col.b * 255;

					int yiq = (r * 299 + g * 587 + b * 114) / 1000;
				
					if (yiq >= 128)
						col.rgb = 0;
					else
						col.rgb = 1;
				}
                return col;
            }
            ENDCG
        }
    }
}
